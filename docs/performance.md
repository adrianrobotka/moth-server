## Load test results

commit / tag                             | size  | med   | avg   | reqs/sec
---------------------------------------- | ----- | ----- | ----- | ---
v0.1                                     | 167 B | 12 ms | 76 ms | 320
2102b1d2415cdc0e568b850ce8ebc9721a88f0eb | 176 B |  9 ms | 21 ms | 328