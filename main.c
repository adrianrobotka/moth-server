/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Entry point file.
 *
 * Handle CLI arguments, signals and worker threads
 *
 */

#include<stdio.h>   // write to stderr
#include<stdlib.h>  // general stuff
#include<string.h>  // strlen method
#include<unistd.h>  // program return code
#include<pthread.h> // threading
#include<syslog.h>  // syslog logging
#include<ctype.h>   // methods with char
#include<signal.h>  // signal handing

#include "server.h"
#include "storage.h"
#include "response.h"

/// Array of threads
pthread_t *threads;

/**
 * Prepare configuration, process CLI arguments
 *
 * @param argc CLI arg counter
 * @param args CLI arg array
 * @param config Writeable configuration (result)
 * @return Fatal error message, NULL if success
 */
const char *init_program(int argc, const char **args, prg_config *config);

/**
 * Show CLI help about program usage
 */
void show_program_usage(void);

/**
 * Run a connection acceptor (per thread method)
 *
 * @return Noting
 */
void *run_worker_thread();

/**
 * Handle a specific signal
 *
 * @param signal_number ID of the signal to handle
 */
void signal_handler(int signal_number);

/**
 * Register all the signal handlers
 */
void register_signal_handlers(void);

/**
 * Entry point of the program
 *
 * @param argc Count of cli arguments
 * @param args Array of the arguments
 * @return Exit code
 */
int main(int argc, char const *args[]) {
    // Configuration of the program
    prg_config config;

    /*
     * Init program
     */
    const char *error_msg = init_program(argc, args, &config);
    if (error_msg != NULL) {
        fprintf(stderr, "%s\n", error_msg);

        show_program_usage();
        return EXIT_FAILURE;
    }

    // Prepare error handling
    register_signal_handlers();

    // Prepare multi-thread working
    size_t core_num = (size_t) sysconf(_SC_NPROCESSORS_ONLN);
    threads = malloc(sizeof(pthread_t) * core_num);

    syslog(LOG_INFO, "Start Moth server on port %s on %zu threads\n",
           config.port, core_num);

    // Bind on port
    open_socket(config.port, strtol(config.do_reuse, NULL, 0));

    for (size_t i = 0; i < core_num; ++i) {
        pthread_create(&threads[i], NULL, run_worker_thread, NULL);
    }

    for (size_t i = 0; i < core_num; ++i) {
        pthread_join(threads[i], NULL);
    }
    free(threads);

    syslog(LOG_INFO, "Moth server stopped\n");

    // freedom
    free_storage();
    free_config(&config);

    closelog();

    return EXIT_SUCCESS;
}

void *run_worker_thread() {
    // Accept all connections with process_request method
    accept_connections();

    return NULL;
}

void show_program_usage(void) {
    fprintf(stderr, "Usage: CONFIG_FILE\n");
}

const char *init_program(const int argc, const char **args,
                         prg_config *config) {
    char *error_message;
    /*
     * Init syslog
     */
    setlogmask(LOG_UPTO (LOG_DEBUG));
    openlog("moth", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

    const char *config_file = "config.cfg";

    // Change default config file
    if (argc > 1) {
        config_file = args[1];

        size_t config_file_len = strlen(config_file);
        for (size_t i = 0; i < config_file_len; ++i)
            // check invalid characters
            if (!isascii(config_file[i])) {
                return "Invalid character in config file name.";
            }
    }

    error_message = read_config_file(config_file, config);

    if (error_message != NULL)
        return error_message;

    switch (read_storage(config->webroot)) {
        case STORAGE_OK:
            break;
        case CANNOT_LIST_STORAGE_FOLDER:
            return "Cannot list files in working dir.";
        case STORAGE_NO_FILES:
            return "There are no files in storage dir.";
        case MORE_FILE_EXPECTED:
            return "Internal error, more storage files are expected.";
        case CANNOT_READ_FILE:
            return "Cannot read a storage file.";
    }

    init_response_factory();

    return NULL;
}

void signal_handler(const int signal_number) {
    syslog(LOG_DEBUG, "CATCH SIGNAL: %d\n", signal_number);
    close_socket();
}

void register_signal_handlers(void) {
    if (signal(SIGHUP, signal_handler) == SIG_ERR)
        syslog(LOG_DEBUG, "can't catch SIGHUP\n");

    if (signal(SIGINT, signal_handler) == SIG_ERR)
        syslog(LOG_DEBUG, "can't catch SIGINT\n");

    if (signal(SIGTERM, signal_handler) == SIG_ERR)
        syslog(LOG_DEBUG, "can't catch SIGTERM\n");

    closelog();
}
